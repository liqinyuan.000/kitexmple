package main

import (
	"context"
	"fmt"

	"code.byted.org/gopkg/logs"
	"code.byted.org/kitex/example_student/kitex_gen/kitex/example/student"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 初始化数据库
func InitDB() *gorm.DB {
	timeout := "10s"            //连接超时，10秒
	var username = "root"       // 用户名
	var password = "123456"     // 密码
	var host = "10.248.139.148" // 数据库host
	var port = "3306"           // 端口
	var Dbname = "demo"         // 数据库名
	var charset = "utf8mb4"     // 字符集
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local&timeout=%s", username, password, host, port, Dbname, charset, timeout)
	fmt.Println("dsn:" + dsn)
	//连接MYSQL, 获得DB类型实例，用于后面的数据库读写操作。
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Printf("数据库请求失败：%v\n", err)
		return nil
	}
	return db
}

// StudentServiceImpl implements the last service interface defined in the IDL.
type StudentServiceImpl struct{}

var tableName = "student"

// GetStudentById implements the StudentServiceImpl interface.
func (s *StudentServiceImpl) GetStudentById(ctx context.Context, req *student.GetStudentByIdRequest) (resp *student.GetStudentByIdResponse, err error) {
	// TODO: Your code here...
	logs.CtxDebug(ctx, "GetStudentById called")
	resp = student.NewGetStudentByIdResponse()
	db := InitDB()
	var studentVar student.Student
	_ = db.Debug().Table(tableName).First(&studentVar, req.Id).Error
	logs.CtxDebug(ctx, "Get Student Success! Name: "+studentVar.Name)
	resp.Student = &studentVar
	return
}

// InsertStudent implements the StudentServiceImpl interface.
func (s *StudentServiceImpl) InsertStudent(ctx context.Context, req *student.InsertStudentRequest) (resp *student.InsertStudentResponse, err error) {
	// TODO: Your code here...
	logs.CtxDebug(ctx, "InsertStudent called")
	resp = student.NewInsertStudentResponse()
	db := InitDB()
	var studentVar student.Student
	studentVar.Id = req.Id
	studentVar.Name = req.Name
	studentVar.Age = req.Age
	_ = db.Debug().Table(tableName).Create(&studentVar).Error
	logs.CtxDebug(ctx, "Insert Sucess! Student Name: "+studentVar.Name)
	resp.Id = studentVar.Id
	resp.Name = studentVar.Name
	resp.Age = studentVar.Age
	return
}

// UpdateStudent implements the StudentServiceImpl interface.
func (s *StudentServiceImpl) UpdateStudent(ctx context.Context, req *student.UpdateStudentRequest) (resp *student.UpdateStudentResponse, err error) {
	// TODO: Your code here...
	logs.CtxDebug(ctx, "UpdateStudent called")
	resp = student.NewUpdateStudentResponse()
	db := InitDB()
	var studentVar student.Student
	studentVar.Id = req.Id
	studentVar.Name = req.Name
	studentVar.Age = req.Age
	_ = db.Debug().Table(tableName).Updates(&studentVar).Error
	logs.CtxDebug(ctx, "Update Sucess! Student Name: "+studentVar.Name)
	resp.Id = studentVar.Id
	resp.Name = studentVar.Name
	resp.Age = studentVar.Age
	return
}

// DeleteStudentById implements the StudentServiceImpl interface.
func (s *StudentServiceImpl) DeleteStudentById(ctx context.Context, req *student.DeleteStudentByIdRequest) (resp *student.DeleteStudentByIdResponse, err error) {
	// TODO: Your code here...
	logs.CtxDebug(ctx, "DeleteStudentById called")
	resp = student.NewDeleteStudentByIdResponse()
	db := InitDB()
	var studentVar student.Student
	studentVar.Id = req.Id
	_ = db.Debug().Table(tableName).Delete(&studentVar).Error
	logs.CtxDebug(ctx, "Delete Student Success! Name: "+studentVar.Name)
	resp.Status = "Success"
	return
}
