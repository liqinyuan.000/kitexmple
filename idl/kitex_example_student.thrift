include "base.thrift"

namespace go kitex.example.student

struct Student {
    1: required i64 id,
    2: required string name,
    3: required i64 age,

    10: optional map<string, string> extra,
}

struct GetStudentByIdRequest {
    1: required i64 id,

    255: optional base.Base Base, // for compatibility with non-kitex framework
}

struct GetStudentByIdResponse {
    1: required Student student,

    255: optional base.BaseResp BaseResp, // for compatibility with non-kitex framework
}

struct InsertStudentRequest {
    1: required i64 id,
    2: required string name,
    3: required i64 age,

    255: optional base.Base Base, // for compatibility with non-kitex framework
}

struct InsertStudentResponse {
    1: required i64 id,
    2: required string name,
    3: required i64 age,

    255: optional base.BaseResp Base, // for compatibility with non-kitex framework
}

struct UpdateStudentRequest {
    1: required i64 id,
    2: required string name,
    3: required i64 age,

    255: optional base.Base Base, // for compatibility with non-kitex framework
}

struct UpdateStudentResponse {
    1: required i64 id,
    2: required string name,
    3: required i64 age,

    255: optional base.BaseResp Base, // for compatibility with non-kitex framework
}

struct DeleteStudentByIdRequest {
    1: required i64 id,

    255: optional base.Base Base, // for compatibility with non-kitex framework
}

struct DeleteStudentByIdResponse {
    1: required string status,

    255: optional base.BaseResp Base, // for compatibility with non-kitex framework
}

service StudentService {
    GetStudentByIdResponse GetStudentById (1: GetStudentByIdRequest req),
    InsertStudentResponse InsertStudent (1: InsertStudentRequest req),
    UpdateStudentResponse UpdateStudent (1: UpdateStudentRequest req),
    DeleteStudentByIdResponse DeleteStudentById (1: DeleteStudentByIdRequest req),
}