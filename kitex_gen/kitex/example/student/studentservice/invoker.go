// Code generated by Kitex v1.9.1. DO NOT EDIT.

package studentservice

import (
	"code.byted.org/kite/kitex/byted"
	"code.byted.org/kite/kitex/server"
	"code.byted.org/kitex/example_student/kitex_gen/kitex/example/student"
)

// NewInvoker creates a server.Invoker with the given handler and options.
func NewInvoker(handler student.StudentService, opts ...server.Option) server.Invoker {
	var options []server.Option

	options = append(options, byted.InvokeSuite(serviceInfo()))

	options = append(options, opts...)

	s := server.NewInvoker(options...)
	if err := s.RegisterService(serviceInfo(), handler); err != nil {
		panic(err)
	}
	if err := s.Init(); err != nil {
		panic(err)
	}
	return s
}

// NewInvokerWithBytedConfig creates a server.Invoker with the given handler and options.
func NewInvokerWithBytedConfig(handler student.StudentService, config *byted.ServerConfig, opts ...server.Option) server.Invoker {
	var options []server.Option
	options = append(options, byted.InvokeSuiteWithConfig(serviceInfo(), config))
	options = append(options, opts...)

	s := server.NewInvoker(options...)
	if err := s.RegisterService(serviceInfo(), handler); err != nil {
		panic(err)
	}
	if err := s.Init(); err != nil {
		panic(err)
	}
	return s
}
