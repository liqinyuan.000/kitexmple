package main

import (
	student "code.byted.org/kitex/example_student/kitex_gen/kitex/example/student/studentservice"
	"log"
)

func main() {
	svr := student.NewServer(new(StudentServiceImpl))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
